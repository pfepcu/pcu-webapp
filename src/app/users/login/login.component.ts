import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'

import { AuthenticationService } from '../../shared/services/authentication.service';
import { HttpSessionService } from '../../shared/helpers/http-session.service';

import { LoginForm } from './login-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public hasSubmissionError: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private httpSessionService: HttpSessionService,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('(([a-z-]+.[a-z]+.[0-9])|[a-z]{2}[0-9]{5})@ens.etsmtl.ca')
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ])]
    });
  }

  save(model: LoginForm, isValid: boolean) {

    let loginOperation = this.authenticationService.login(model);

    loginOperation.subscribe(
      response => {
        this.httpSessionService.createSession();
        this.router.navigateByUrl('/');
      },
      error => {
        this.hasSubmissionError = true;
      }
    );

    return false;
  }
}
