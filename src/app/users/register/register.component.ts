import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'

import { UserService } from '../../shared/services/user.service';
import { ProgramService } from '../../shared/services/program.service';
import { ConcentrationService } from '../../shared/services/concentration.service';

import { RegisterForm } from './register-form';
import { SessionService } from '../../shared/services/session.service';
import { SeasonEnum } from '../../shared/models/season-enum';
import { ProgramEnum } from '../../shared/models/program-enum';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  public programs;
  public concentrations;
  public currentProgramConcentrations;
  public sessions;
  public seasonEnum = SeasonEnum;

  public hasSubmissionError: boolean = false;

  constructor(private userService : UserService,
              private programService: ProgramService,
              private sessionService: SessionService,
              private concentrationService: ConcentrationService,
              private formBuilder: FormBuilder,
              private router: Router) {}

    ngOnInit() {

      this.registerForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.compose([
          Validators.required,
          Validators.pattern('(([a-z-]+.[a-z]+.[0-9])|[a-z]{2}[0-9]{5})@ens.etsmtl.ca')
        ])],
        programId: ['', Validators.required],
        concentrationId: ['', Validators.required],
        successMatTest: [false, Validators.required],
        successPhyTest: [false, Validators.required],
        successInfTest: [false, Validators.required],
        startingSessionId: ['', Validators.required],
        password: ['', Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50)
        ])]
      });

      this.programService.getAll().subscribe(
        programs => {
          this.programs = programs;
        }
      );

      this.concentrationService.getAll().subscribe(
        concentrations => {
          this.concentrations = concentrations;
        }
      );

      this.sessionService.getAll().subscribe(
        sessions => {
          this.sessions = sessions;
        }
      );
    }

    getCurrentProgramConcentrations() {
      if(this.registerForm.value.programId != null && this.concentrations != null) {
        this.currentProgramConcentrations = this.concentrations.filter(concentration => concentration.idProgram == this.registerForm.value.programId);
      }
    }

    isLogicielOrTiProgram(){
      return (this.registerForm.value.programId == ProgramEnum.LOGICIEL?true:false);
    }

    save(model: RegisterForm, isValid: boolean) {

      let registerOperation = this.userService.register(model);

      registerOperation.subscribe(
        response => {
          this.router.navigateByUrl('/users/login');
        },
        error => {
          this.hasSubmissionError = true;
        }
      );

      return false;
    }
  }
