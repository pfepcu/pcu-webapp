export interface RegisterForm {
  firstName: string,
  lastName: string,
  email: string,
  program: string,
  concentration: string,
  successMatTest: boolean,
  successPhyTest: boolean,
  successInfTest: boolean,
  startingSession: string,
  password: string
}
