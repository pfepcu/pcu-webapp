export interface ProfileCourse {
  courseId: number,
  courseAcronym: string,
  checked: boolean
}
