import { Component, OnInit } from '@angular/core';
import { ProgramService } from '../../shared/services/program.service';
import { ProgramRevisionService } from '../../shared/services/program-revision.service';
import { SessionService } from '../../shared/services/session.service';
import { SessionCourseStudentService } from '../../shared/services/session-course-student.service';
import { Program } from '../../shared/models/program';
import { Course } from '../../shared/models/course';
import { ProfileCourse } from './profile-course';
import { Session } from '../../shared/models/session';
import { SessionCourse } from '../../shared/models/session-course';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { User } from '../../shared/models/user';
import { SeasonEnum } from '../../shared/models/season-enum';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public program: Program;
  public session: Session;
  public user: User;
  public programCourses: ProfileCourse[] = [];
  public pastSessionCourses: SessionCourse[];
  public seasonEnum = SeasonEnum;

  constructor(private authenticationService : AuthenticationService,
              private programService: ProgramService,
              private programRevisionService: ProgramRevisionService,
              private sessionService: SessionService,
              private sessionCourseStudentService: SessionCourseStudentService) { }

  ngOnInit() {
    this.authenticationService.me().subscribe(
      user => {

        this.user = user;

        this.programService.get(this.user.programId).subscribe(
          program => {
            this.program = program;
          }
        );

        this.sessionService.get(this.user.startingSessionId).subscribe(
          session => {
            this.session = session;
          }
        );

        this.programRevisionService.getUserCourses().subscribe(
          courses => {
            this.populateProgramCourses(courses);

            this.sessionCourseStudentService.getPastSessionCourses().subscribe(
              sessionCourses => {
                this.pastSessionCourses = sessionCourses;
                this.updateCheckedProgramCourses();
              }
            );
          }
        );
      }
    );
  }

  public togglePastSessionCourse(profileCourse: ProfileCourse) {
   if(profileCourse.checked) {
     this.removePastSessionCourse(profileCourse.courseId)
      .subscribe(_ => profileCourse.checked = false); 
   } else {
     this.addPastSessionCourse(profileCourse.courseId)
      .subscribe(_ => profileCourse.checked = true); 
   }
  }

  private populateProgramCourses(courses : Course[]) {
    courses.forEach(course => {
      let profileCourse : ProfileCourse = {
        courseId: course.id,
        courseAcronym: course.acronym,
        checked: false
      };
      this.programCourses.push(profileCourse);
    });
  }

  private updateCheckedProgramCourses() {
    for (var i = 0; i < this.programCourses.length; i++) {
      let acronym = this.programCourses[i].courseAcronym;

      let matchingCourses = this.pastSessionCourses.filter(pastSessionCourse => {
        return pastSessionCourse.course.acronym === acronym;
      });

      this.programCourses[i].checked = !!matchingCourses.length;
    }
  }

  private addPastSessionCourse(courseId) {
    return this.sessionCourseStudentService.addPastSessionCourse(courseId);
  }

  private removePastSessionCourse(courseId) {
    return this.sessionCourseStudentService.removePastSessionCourse(courseId);
  }
}
