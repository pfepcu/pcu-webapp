import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from "./profile/profile.component";
import { PreferencesComponent } from "./preferences/preferences.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsersRoutingModule
  ],
  declarations: [
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    PreferencesComponent
  ],
  providers: [],
  exports: [
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    PreferencesComponent
  ]
})
export class UsersModule { }
