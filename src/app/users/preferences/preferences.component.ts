import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PreferencesService } from "../../shared/services/preferences.service";
import { PreferencesForm } from "./preferences-form";
import { Router } from "@angular/router";

@Component({
  selector: 'app-preference',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit {

  public preferencesForm: FormGroup;
  public hasSubmissionError: boolean = false;

  constructor(private preferencesService : PreferencesService,
              private formBuilder: FormBuilder,
              private router: Router) {}

  ngOnInit() {

    this.preferencesForm = this.formBuilder.group({
      id: [1, Validators.required],
      minSessionCourse: [4, Validators.required],
      maxSessionCourse: [4, Validators.required],
      maxEveningCourse: [2, Validators.required],
      courseDuringInternship: [false, Validators.required],
      eagerInternship: [true, Validators.required],
      maxCourseDuringInternship: [0, Validators.required],
      sessionBetweenInternship: [2, Validators.required],
      maxSessionToComplete: [11, Validators.required]
    }, PreferencesComponent.sessionCourseValidator);

    this.preferencesService.getUserPreferences().subscribe(
      preferences => {
        this.preferencesForm.setValue(preferences);
      },
    error => console.error(error)
    );

  };

  static sessionCourseValidator(formGroup: FormGroup){
    return formGroup.get('minSessionCourse').value <= formGroup.get('maxSessionCourse').value;
  }

  save(model: PreferencesForm, isValid: boolean) {
    let updateOperation = this.preferencesService.updateUserPreferences(model);

    updateOperation.subscribe(
      response => {
        this.router.navigateByUrl('/users/profile');
      },
      error => {
        this.hasSubmissionError = true;
      }
    );

    return false;
  }
}
