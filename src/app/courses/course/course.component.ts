import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { CourseService } from "../../shared/services/course.service";
import { Course } from "../../shared/models/course";

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, OnDestroy {

  private course: Course;
  private sub: any;

  constructor(private route: ActivatedRoute, private courseService: CourseService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = Number.parseInt(params['id']);
      this.courseService.get(id).subscribe(course => this.course = course);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
