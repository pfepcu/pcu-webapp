import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseComponent } from './course/course.component';
import { CoursesRoutingModule } from "./courses-routing.module";

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule
  ],
  declarations: [
    CourseComponent
  ],
  providers: [

  ],
  exports: [
    CourseComponent
  ]
})
export class CoursesModule { }
