import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: 'users', loadChildren: 'app/users/users.module#UsersModule' },
  { path: 'pathways', loadChildren: 'app/pathways/pathways.module#PathwaysModule' },
  { path: 'courses', loadChildren: 'app/courses/courses.module#CoursesModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
