import { Injectable } from '@angular/core';

import { User } from '../models/user';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class HttpSessionService {

  loggedInUser : User;

  constructor(private authenticationService : AuthenticationService) { }

  createSession() : User {
    if(!this.loggedInUser) {
      this.authenticationService.me()
        .subscribe(user => {
          this.loggedInUser = user;
          return this.loggedInUser;
        });
    } else {
      return this.loggedInUser;
    }
  }

  destroySession() {
    this.authenticationService.logout();
    this.loggedInUser = null;
  }

  hasLoggedInUser() : boolean {
    return !!this.loggedInUser;
  }
}
