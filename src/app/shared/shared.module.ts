import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpSessionService } from './helpers/http-session.service';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { ProgramService } from './services/program.service';
import { ProgramRevisionService } from './services/program-revision.service';
import { PathwayService } from "./services/pathway.service";
import { SessionService } from "./services/session.service";
import { SessionCourseStudentService } from "./services/session-course-student.service";
import { ConcentrationService } from "./services/concentration.service"
import { CourseService } from "./services/course.service";
import { PreferencesService } from "./services/preferences.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    HttpSessionService,
    AuthenticationService,
    UserService,
    ProgramService,
    ProgramRevisionService,
    PathwayService,
    SessionService,
    SessionCourseStudentService,
    CourseService,
    ConcentrationService,
    PreferencesService
  ]
})
export class SharedModule { }
