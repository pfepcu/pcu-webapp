import { StudentSession } from "./student-session";

export interface Pathway {
  studentSessions: StudentSession[]
}
