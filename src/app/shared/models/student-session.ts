import { SessionCourse } from "./session-course";
import { SeasonEnum } from "./season-enum";

export interface StudentSession {
  id: number,
  season: SeasonEnum,
  year: number,
  sessionCourses: SessionCourse[]
}
