export interface User {
  firstName: string,
  lastName: string,
  email: string,
  programId: number,
  startingSessionId: number,
  password: string
}
