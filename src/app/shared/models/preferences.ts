export interface Preferences {
  id: number,
  minSessionCourse: number,
  maxSessionCourse: number,
  maxEveningCourse: number,
  courseDuringInternship: boolean,
  eagerInternship: boolean,
  maxCourseDuringInternship: number,
  sessionBetweenInternship: number,
  maxSessionToComplete: number
}
