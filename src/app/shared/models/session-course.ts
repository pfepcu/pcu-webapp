import { Course } from "./course";

export interface SessionCourse {
  id: number,
  course: Course,
  day: boolean,
  evening: boolean,
  locked: boolean
}
