export interface Concentration {
  id: number,
  idProgram: number,
  description: string
}
