export interface Course {
  id: number,
  name: string,
  acronym: string,
  description: string,
  credits: number,
  requiredCredits: number,
  prerequisites: Course[],
  successor: Course
}
