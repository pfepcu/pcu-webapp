import { SeasonEnum } from "./season-enum";

export interface Session {
  id: number,
  season: SeasonEnum,
  year: number
}
