import { Injectable } from '@angular/core';

import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { Course } from '../models/course';

@Injectable()
export class ProgramRevisionService {

  private baseUrl: string = `${environment.core.url}/programRevisions`;

  constructor(private http: Http) { }

  getUserCourses() : Observable<Course[]> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/courses`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
