import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';
import { Preferences } from '../models/preferences';
import { PreferencesForm } from "../../users/preferences/preferences-form";

@Injectable()
export class PreferencesService {

  private baseUrl: string = `${environment.core.url}/preferences`;

  constructor(private http: Http) { }

  getUserPreferences() : Observable<Preferences> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.baseUrl, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateUserPreferences(preferencesForm : PreferencesForm) : Observable<Response> {

    let bodyString = JSON.stringify(preferencesForm);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.put(this.baseUrl, bodyString, options)
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
