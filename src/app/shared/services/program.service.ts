import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { Program } from '../models/program';

@Injectable()
export class ProgramService {

  private baseUrl: string = `${environment.core.url}/programs`;

  constructor(private http: Http) { }

  getAll() : Observable<Program> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${this.baseUrl}/all`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  get(id) : Observable<Program> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/${id}`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
