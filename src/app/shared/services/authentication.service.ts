import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { LoginForm } from '../../users/login/login-form';

import { User } from '../models/user';

@Injectable()
export class AuthenticationService {

  private baseUrl: string = `${environment.core.url}/auth`;

  constructor(private http: Http) {}

  me() : Observable<User> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/me`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  login(loginForm : LoginForm) : Observable<Response> {

    let bodyString = JSON.stringify(loginForm);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(`${this.baseUrl}/login`, bodyString, options)
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  logout() : Observable<Response> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/logout`, options)
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
