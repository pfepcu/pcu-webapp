import { Injectable } from '@angular/core';
import { Course } from "../models/course";
import { Observable } from "rxjs";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { environment } from "../../../environments/environment";

@Injectable()
export class CourseService {

  private baseUrl: string = `${environment.core.url}/courses`;

  constructor(private http: Http) { }

  get(id) : Observable<Course> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/${id}`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
