import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { RegisterForm } from "../../users/register/register-form";
import { Observable } from "rxjs";
import { Pathway } from "../models/pathway";

@Injectable()
export class PathwayService {

  private baseUrl: string = `${environment.core.url}/pathways`;

  constructor(private http: Http) {}

  get(): Observable<Pathway>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  generate(startingSession, internshipSession): Observable<Pathway>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = {
      startingSessionId: startingSession,
      nextInternshipSessionId: internshipSession
    };

    return this.http.put(`${this.baseUrl}/generate`, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  lockCourse(id: number) : Observable<Response>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = "";

    return this.http.put(`${this.baseUrl}/lockcourse/${id}`, body, options);
  }

  unlockCourse(id: number) : Observable<Response>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = "";

    return this.http.put(`${this.baseUrl}/unlockcourse/${id}`, body, options);
  }

  lockSession(id: number) : Observable<Response>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = "";

    return this.http.put(`${this.baseUrl}/locksession/${id}`, body, options);
  }

  unlockSession(id: number) : Observable<Response>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = "";

    return this.http.put(`${this.baseUrl}/unlocksession/${id}`, body, options);
  }
}
