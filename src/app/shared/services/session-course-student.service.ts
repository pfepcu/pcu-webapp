import { Injectable } from '@angular/core';

import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { SessionCourse } from '../models/session-course';

@Injectable()
export class SessionCourseStudentService {

  private baseUrl: string = `${environment.core.url}/sessionCourseStudents`;

  constructor(private http: Http) { }

  getPastSessionCourses() : Observable<SessionCourse[]> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/past`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  addPastSessionCourse(courseId) : Observable<Response> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = "";

    return this.http.post(`${this.baseUrl}/past/${courseId}`, body, options);
  }

  removePastSessionCourse(courseId) : Observable<Response> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.delete(`${this.baseUrl}/past/${courseId}`, options);
  }

}
