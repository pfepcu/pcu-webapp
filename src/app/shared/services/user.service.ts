import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { RegisterForm } from '../../users/register/register-form';

@Injectable()
export class UserService {

  private baseUrl: string = `${environment.core.url}/users`;

  constructor(private http: Http) {}

  register(registerForm : RegisterForm) : Observable<Response> {

    let bodyString = JSON.stringify(registerForm);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.baseUrl, bodyString, options)
       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
