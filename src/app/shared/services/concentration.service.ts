import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { Concentration } from '../models/concentration';

@Injectable()
export class ConcentrationService {

  private baseUrl: string = `${environment.core.url}/concentrations`;

  constructor(private http: Http) { }

  getAll() : Observable<Concentration> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${this.baseUrl}/all`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
