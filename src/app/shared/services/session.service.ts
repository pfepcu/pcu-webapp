import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { Injectable } from '@angular/core';

import { Observable } from "rxjs";

import { environment } from "../../../environments/environment";

import { Session } from "../models/session";

@Injectable()
export class SessionService {

  private baseUrl: string = `${environment.core.url}/sessions`;

  constructor(private http: Http) { }

  getAll() : Observable<Session> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${this.baseUrl}/all`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  get(id) : Observable<Session> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(`${this.baseUrl}/${id}`, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
