import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { PathwaysRoutingModule } from './pathways-routing.module';
import { PathwayComponent } from "./pathway/pathway.component";

@NgModule({
  imports: [
    CommonModule,
    PathwaysRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    PathwayComponent
  ],
  providers: [

  ],
  exports: [
    PathwayComponent
  ]
})
export class PathwaysModule { }
