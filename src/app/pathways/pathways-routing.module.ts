import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PathwayComponent } from './pathway/pathway.component';

const routes: Routes = [
  { path: 'pathway', component: PathwayComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PathwaysRoutingModule {}
