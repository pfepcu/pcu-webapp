import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PathwayService } from '../../shared/services/pathway.service';
import { Pathway } from '../../shared/models/pathway';
import { StudentSession } from "../../shared/models/student-session";
import { SeasonEnum } from "../../shared/models/season-enum";
import { SessionCourse } from "../../shared/models/session-course";
import { SessionService } from "../../shared/services/session.service";

@Component({
  selector: 'app-pathways',
  templateUrl: './pathway.component.html',
  styleUrls: ['./pathway.component.css']
})
export class PathwayComponent implements OnInit, OnDestroy {

  private pathwayServiceGetSubscribe: any;
  private pathwayServiceGenerateSubscribe: any;
  private pathwayServiceLockCourseSubscribe: any;
  private pathwayServiceUnlockCourseSubscribe: any;
  private pathwayServiceLockSessionSubscribe: any;
  private pathwayServiceUnlockSessionSubscribe: any;
  public studentSessions: StudentSession[];
  public seasonEnum = SeasonEnum;
  public sessions;
  public startingSession;
  public internshipSession;

  constructor(private pathwayService : PathwayService,
              private sessionService: SessionService) { }

  ngOnInit() {
    this.pathwayServiceGetSubscribe = this.pathwayService.get()
      .subscribe(pathway => this.studentSessions = pathway.studentSessions,
        error => console.error(error));

    this.sessionService.getAll().subscribe(
      sessions => {
        this.sessions = sessions;
      }
    );

    this.startingSession = 1;
    this.internshipSession = "";
  }

  ngOnDestroy(): void {
    this.pathwayServiceGetSubscribe.unsubscribe();
  }

  generate() {
    this.pathwayServiceGenerateSubscribe = this.pathwayService.generate(this.startingSession, this.internshipSession)
      .subscribe(pathway => this.studentSessions = pathway.studentSessions,
        error => console.error(error));
  }

  lockCourse(sessionCourse : SessionCourse) {
    this.pathwayServiceLockCourseSubscribe = this.pathwayService.lockCourse(sessionCourse.id).subscribe(res => {
      if(res.status == 200){
        sessionCourse.locked = true;
      }
    });
  }

  unlockCourse(sessionCourse : SessionCourse) {
    this.pathwayServiceUnlockCourseSubscribe = this.pathwayService.unlockCourse(sessionCourse.id).subscribe(res => {
      if (res.status == 200) {
        sessionCourse.locked = false;
      }
    });
  }

  lockSession(studentSession : StudentSession) {
    this.pathwayServiceLockSessionSubscribe = this.pathwayService.lockSession(studentSession.id).subscribe(res => {
      if(res.status == 200){
        studentSession.sessionCourses.forEach(sessionCourse => sessionCourse.locked = true);
      }
    });
  }

  unlockSession(studentSession : StudentSession) {
    this.pathwayServiceUnlockSessionSubscribe = this.pathwayService.unlockSession(studentSession.id).subscribe(res => {
      if(res.status == 200){
        studentSession.sessionCourses.forEach(sessionCourse => sessionCourse.locked = false);
      }
    });
  }

  checkSessionIsLocked(studentSession : StudentSession) {
    let sessionLocked = true;
    studentSession.sessionCourses.forEach(sessionCourse => sessionLocked = !sessionCourse.locked ? false : sessionLocked);
    return sessionLocked;
  }
}
