import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { HttpSessionService } from './shared/helpers/http-session.service';
import { User } from './shared/models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private httpSessionService : HttpSessionService,
              private router : Router) {
    this.httpSessionService.createSession();
  }

  hasLoggedInUser() : boolean {
    return this.httpSessionService.hasLoggedInUser();
  }

  getLoggedInUser() : User {
    return this.httpSessionService.loggedInUser;
  }

  logout() {
    this.httpSessionService.destroySession();
    this.router.navigateByUrl('/');
  }
}
